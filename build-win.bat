@ECHO OFF
REM Compile MBR, VBR and kernel
tools-win\nasm.exe mbr.asm -o mbr.bin > build.log 2>&1
tools-win\nasm.exe vbr.asm -o vbr.bin >> build.log 2>&1
tools-win\nasm.exe kernel.asm -o kernel.bin >> build.log 2>&1

REM Compile core
tools-win\nasm.exe core\core.asm -o fsgen\rootfs\core

REM Generate file system
fsgen\fsgen.exe fsgen\rootfs rootfs.hdd 256

REM Generate final hard disk image
tools-win\cat.exe mbr.bin vbr.bin kernel.bin rootfs.hdd > image.hdd 2>> build.log

REM Clean up
del *.bin

REM Start QEMU
tools-win\qemu.exe -hda image.hdd >> build.log 2>&1
