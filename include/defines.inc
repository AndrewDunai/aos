%define FLFS_TEMP	0x7C00
;%define KERNEL_ENTRY	0x7E00

%define KERNEL_SEG	0x07E0

;%define VBR_ADDR	0x6000
%define VBR_SEG		0x0050
%define VBR_ADDR	0x0000

struc PartitionRecord
	boot:	resb	1
	start_head:	resb	1
	start_sector:	resb	1
	start_cylinder:	resb	1
	part_type:	resb	1
	end_head:	resb	1
	end_sector:	resb	1
	end_cylinder:	resb	1
	start_lba:	resd	1
	secnum:	resd	1
endstruc

struc FLFS_Node
	.type:	resb	1
	.data:	resb	495
	.child:	resq	1
	.next:	resq	1
endstruc

%define	node_type(X) byte [X]
%define	node_data(X) X+FLFS_Node.data
%define	node_child(X) qword [X+FLFS_Node.child]
%define	node_next(X) qword [X+FLFS_Node.next]

%define	node_child_ms(X) dword [X+FLFS_Node.child]
%define	node_child_ls(X) dword [X+FLFS_Node.child+4]
%define	node_next_ms(X) dword [X+FLFS_Node.next]
%define	node_next_ls(X) dword [X+FLFS_Node.next+4]

%macro _comp 2
	mov	ah,	0
	mov	ebx,	%1
	mov	edx,	%2
	int	40h
%endmacro
