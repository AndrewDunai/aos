; FLoating File System definition
; All FS consists of same type blocks

struc FFS_BLOCK
	.type	resb	1	; 1	entry type:
				;	bits 3..2: 10 - is ROOT, 00 - is DIR, 01 - is FILE
				;	bits 1..0:
				;	1 - sector is FFS_ENTRY
				;	0 - sector is FFS_NODE
	.name	resb	495	; 495	entry name (if FFS_ENTRY) or file data (if FFS_NODE)
	.child	resq	1	; 8	child entry (points to sub-items (if DIR or ROOT) or to file data (if FILE))
	.next	resq	1	; 8	next entry in current level
endstruc
