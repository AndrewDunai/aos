%define TIMER_0_PORT	0x40	; i8254 timer 1st channel
%define TIMER_1_PORT	0x41	; i8254 timer 2nd channel
%define TIMER_2_PORT	0x42	; i8254 timer 3rd channel
%define TIMER_CTRL_PORT	0x43	; i8254 Control register

%define TIMER_0_M0	00110000b	; = 0x30
%define TIMER_1_M0	00110000b	; = 0x70

%define TIMER_BASE	1193180.0	; i8254 frequency
%define	TIMER_FREQ	TIMER_BASE/18.2	; =0xFFFF; We want 18.2Hz, so we use 0xFFFF

;asd	db	'asd',0

; Called by i8254 hardware interrupt
int1Ch_handler:
;	mov	ah,	1h
;	mov	si,	asd
;	int	30h
iret
