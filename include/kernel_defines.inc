%define MAX_COMMAND_LEN 127

;	Struct to store console commands
struc ConComm
	.addr	resw	1
	.commlen	resw	1
	.command	resb	32
endstruc
