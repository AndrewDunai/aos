PrintVendor:
	pusha
	xor	eax,	eax
	cpuid
	push	ecx
	push	edx
	push	ebx		; put 12 vendor bytes (ECX:EDX:EBX) into stack

	push	bp		; save BP
	mov	bp,	sp	; set BP to point at stack
	mov	cx,	0x000C	; 12 letters
.cpuid1:
	add	bp,	1	; increase base pointer
	mov	al,	byte [bp+1]	; get next char
	mov	ah,	2h
	int	30h		; print char
	loop	.cpuid1		; print 12 times

	pop	bp
	add	sp,	12	; free stack
	popa
ret

PrintConsolePrefix:
	push esi
	push eax
	mov	ah,	1	; function 1h
	mov	si,	console_prefix	; string buffer
	int	30h		; print string
	pop eax
	pop esi
ret
