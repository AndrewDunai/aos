int_20h:

	push	ax

	cmp	ah,	0h
	je	.fn0
	dec	ah
	jz	.fn1
	dec	ah
	jz	.fn2

.fn0:
	; Does nothing
	nop
	 nop
	  nop
	   nop
	    nop

.fn1:
	; Prints character with specified attributes
	;
	; INPUT:
	;	AH - 1h
	;	DL - character code
	;	DH - character attributes
	;	CL - column (X offset)
	;	CH - row (Y offset)
	;
	; OUTPUT:
	;	(none)

	push	edi
	push	eax
	push	ebx

	mov	edi,	0xB8000		; video memory

	xor	eax,	eax
	xor	ebx,	ebx		; clear EAX/EBX

	mov	al,	ch
	mov	bl,	80d
	mul	bl			; multiply CH by 80 (Character Y offset)

	add	al,	cl		; add character X offset
	adc	ah,	0

	push	edx			; save EDX here because it will be overwritten by 'MUL reg16'

	mov	bx,	2d		; multiply result by 2 (because 2 bytes are needed for each char in videomem)
	mul	bx
	
;	jmp	$

	add	edi,	eax		; EDI = VIDEOMEM + OFFSET

	pop	edx

	mov	[edi],	dx

	pop	ebx
	pop	eax
	pop	edi
	jmp	.end

.fn2:
	; Prints string to screen with specified attributes


.end:
	pop	ax
	iret
