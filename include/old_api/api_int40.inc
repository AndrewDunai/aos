; Interrupt 40h: work with data

int40h_handler:
	push	i40h_end

	cmp	ah,	0h
	je	i40_0
	dec	ah
	jz	i40_1

	ret

; AH=0: Compare two strings
;	Input:		EBX - first string pointer
; 			EDX - second string pointer
;	Returns:	AL = 1 if strings are equal
;			AL = 0 otherwise
;	Modifies:	AL

	i40_0:
		push	ebx		; save registers
		push	edx

		jmp	.start

	.rep:	test	ah,	ah	; if ah=0...
		jz	.eq		; ...then we're done. Strings are identical
		inc	ebx
		inc	edx

	.start:	mov	ah,	[ebx]
		mov	al,	[edx]
		cmp	ah,	al
		je	.rep

	.neq:	mov	al,	0
		jmp	.done
	.eq:	mov	al,	1

		; now return al
	.done:

		pop	edx		; restore registers
		pop	ebx
	ret

; AH=1: Get string length
;	Input:		EBX - string pointer
;			DL - string terminator
;	Returns:	AX - string length
;	Modifies:	AX

	i40_1:
		push	ebx
		xor	ax,	ax
		jmp	.start

	.rep:	inc	ax			; increase length
		inc	ebx			; increase index

	.start:	cmp	byte [ebx],	dl	; if not end of line...
		jne	.rep			; retrieve next byte
		pop	ebx
	ret

	i40h_end:
iret
