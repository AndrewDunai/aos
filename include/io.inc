; printString
; prints string
; parameters:
;	SI - address of zero-ending string buffer
printString:
	push	ax
	push	bx
	mov	ah,	0xE
	mov	bl,	0x7
.next:
	lodsb
	or	al,	al
	jz	.return
	int	10h
	jmp	.next
.return:
	pop	bx
	pop	ax
	ret

; printString (safe mode)
; prints string
; parameters:
;	ESI - address of zero-ending string buffer
;	CL:CH - string start coordinates (X:Y)
;_@printString:
;ret
