;Parameters:
;	AX - pointer to command line string
;	BX - string lenght
ProcessCommand:
	push esi
	push eax
	push ebx
	push ecx
	push edx
	mov	cx,	bx
	mov	bx,	.commands
	mov	dx,	[bx+ConComm.commlen]
	cmp	dx,	cx
	jne	.skip
	mov	si,	ax
	mov	dx,	bx
	add	dx,	ConComm.command
	mov	di,	dx
	;dec cx
	repe	cmpsb
	jne	.skip
	jmp	[bx]
.skip:	mov	ah,	1	; function 1h
	mov	si,	unknown_command	; string buffer
	int	30h		; print string
	pop edx
	pop ecx
	pop ebx
	pop eax
	pop esi
ret
	
	.restart:
	int 19h
ret
	
	.printololo:
	mov	si,	ololo
	mov	ah,	1h
	int	30h
ret

.commands:
	istruc ConComm
	at	ConComm.addr,	dw	.restart
	at	ConComm.commlen,	dw	7
	at	ConComm.command,	db	"restart"
	iend
	istruc ConComm
	at	ConComm.addr,	dw	.printololo
	at	ConComm.commlen,	dw	10
	at	ConComm.command,	db	"printololo"
	iend

	ololo	db	'OLOLO!',0Dh,0Ah,0;
