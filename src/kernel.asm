code	equ	0x8
data	equ	0x10

[BITS 16]
[ORG 0x7E00]
;[ORG 0x0000]
;[ORG 0x0000]

;	org	0x7E00

;	jmp	$
	jmp	kernelStart

%include "include/defines.inc"
%include "include/io.inc"

; kernel-specific
%include "include/kernel_defines.inc"
;%include "include/ctrlbreak_int1Bh.inc"
;%include "include/timer_int1Ch.inc"

;%include "include/api_int30.inc"
;%include "include/api_int31.inc"
;%include "include/api_int32.inc"
;%include "include/api_int40.inc"
%include "include/aux_proc.inc"
%include "include/console.inc"
%include "include/kernel_idt.inc"

kernelStart:

	mov	[disknum],	dl

;mov	ax,	KERNEL_SEG
;mov	cs,	ax
;mov	ds,	ax
;mov	ss,	ax

;	xor	eax,	eax
;	mov	cs,	ax
;	mov	ds,	ax
;	mov	es,	ax
;	mov	fs,	ax
;	mov	gs,	ax
;	mov	ss,	ax

;	mov	ax,	cs
;	mov	ds,	ax
;mov	es,	ax
;mov	ss,	ax

;	mov	ax,	0xB800
;	mov	gs,	eax

	mov	si,	hello
	call	printString

;	jmp	$

;;	mov	[gs:100],	byte 'A'
;;	mov	[gs:101],	byte 0Ah

;	mov	cx,	0x1337
;	mov	dx,	gdt_ptr-(KERNEL_SEG*16)

;	jmp	$

; Here we mess with the protected mode.

;	mov	eax,	gdt
;	jmp	$

	cli			; disable interrupts
	lidt	[idt_ptr]	; Loadint IDT...
	mov	eax,	cr0	; Read CR0
	or	eax,	1	; Set PM flag
	mov	cr0,	eax	; Switch to PM


	jmp	code:stage3
;	jmp	0x8:(stage3+0x7E00)
;	jmp	$

[BITS 32]

stage3:
	mov	ax,	0x10
	mov	ds,	ax
;	hlt
;	mov	ss,	ax
	mov	es,	ax
	mov	esp,	0xFFFFF

;	mov	ah,	1h
;	mov	dl,	'A'
;	mov	dh,	0Ah
;	mov	cl,	10
;	mov	ch,	5
;	int	20h
;	jmp	$

	; awesome test! we put '3' into
	; memory cell at address 100,000,000 bytes
	; and the we read from there! :3
	;; mov	[100000000d], byte '3'
	;; mov	al,	[100000000d]
	;;
	;; mov	edi,	0xB8000	; start of video memory
	;; mov	[edi+1500],	al
	;; mov	[edi+1501],	byte 0Ah

;	xor	ebx,	ebx
;	xor	ecx,	ecx
;	xor	edx,	edx

; Magic break
    xchg    bx, bx

	mov	esi,	hello2

	xor	eax,	eax
	xor	ebx,	ebx
	mov	edi,	0xB8000
	mov	eax,	(10*80 + 11) * 2
	add	edi,	eax

loop:
	lodsb
	cmp	al,	0
	je	.done
	inc	ebx
	mov	[edi],	al
	mov	[edi+1],	byte 0Ah
	add	edi,	2
	jmp	loop
.done:

	mov	edi,	0xB8000
	mov	edx,	edi
	add	edx,	3998d

mainloop:
    jmp mainloop

cls:
	cmp	edi,	0xB8000+4000d
	je	.done
	mov	[edi],	byte 0h
	mov	[edi+1],	byte	0xAA
	mov	[edx],	byte 0h
	mov	[edx+1],	byte	0xAA

;	mov	[edi+2],	byte 0h
;	mov	[edi+3],	byte	0xCA
;	mov	[edi+4],	byte 0h
;	mov	[edi+5],	byte	0xCA
;wait
	add	edi,	2
	sub	edx,	2

	mov	ecx,	0x1FFFF
.again:						; does nothing, makes pause for 0x1FFFF*(.done-.again) cycles
	dec	ecx
	test	ecx,	ecx
	jne	.again

	jmp	cls
.done:


; here we print PM text
	mov	edi,	0x8B000+(40+12*80)*2	; line 12, column 40
	mov	ebx,	edi
print_pm_text:

	cmp	edi,	0x8B000+(0+12*80)*2	; line 12, column 0
	je	.done

	mov	[edi],	byte 'D'
	mov	[edi+1],	byte 0x35

	mov	ecx,	0x1FFFF
.again:						; does nothing, makes pause for 0x1FFFF*(.done-.again) cycles
	dec	ecx
	test	ecx,	ecx
	jne	.again

	sub	edi,	2

	jmp	print_pm_text
.done:


jmp	$

; set data for new kernel
;	mov	ax,	KERNEL_ENTRY	; set DS to point at new kernel
;	mov	ds,	ax	;	...
;	mov	es,	ax
;	mov	ss,	ax

;	mov	ah,	0xE
;	mov	bl,	0x7
;	mov	al,	'A'

;	mov	byte [0x00010000],	0xC3
;	call	[0x00010000]

	mov	edi,	0xB8000
	mov	[edi+1502], byte 'B'
	mov	[edi+1503], byte 0Ah

	jmp	$

;	jmp	KERNEL_ENTRY

; jump to new kernel that was read from HDD
;	jmp	KERNEL_ENTRY:0000h

; function to load kernel from HDD
;	readSector:
;		mov     [word lba_dap+4],	si	; where to place
;		mov	[word lba_dap+6],	ds
;		mov	[dword lba_dap+8],	ebx	; address (bits 63..32)
;		mov	[dword lba_dap+12],	edx	; address (bits 31..0)
;		mov	ah,	42h
;		mov	dl,	[disknum]
;		mov	si,	lba_dap
;		int	13h
;		ret
;	ret
; data
;	lba_dap	db	10h	; used by function int32h:10h
;		db	0h
;		dw	1h
;		dw	0h		; sector offset (sector)
;		dw	0h	; sector segment (KERNEL_ENTRY)
;		dq	0000000000000000h	; what sector to read?
; function end

;[BITS 16]



; Jump to kernel start (skip functions)

;; print test char
;	mov	ah,	0xE	; print char
;	mov	al,	'A'
;	mov	bh,	0x0	; page
;	mov	bl,	0x7	; style
;	int	10h


;[BITS 16]

;	db	10001111b

%include "include/isr20h.inc"

	; [OLD] address 98a here
	welcome	db	" # VBR Boot Loader",13,10,0
	hello	db	" > AOS kernel v0.0 alpha.",13,10," > Created by Anderson & Alexx",13,10," > Greetings!",13,10,0
	hello2	db	" *** We're now in a fully functional protected mode. ***",0
	entry	db	" > Kernel located at physical address ",0
	vendor	db	" > System running at ",0
	fsinfo	db	" * File system contents:",13,10,0
	console_prefix	db	" < ",0
	unknown_command	db	" > Command or file not found",13,10,0
	CommandLine	times (MAX_COMMAND_LEN) db 0,0
	crlf	db	13,10,0
	disknum	db	0	; disk number

	str1	db	'test',0
	str2	db	'test',0	; for string comparation test

	retcode	db	'Return code:',0

	path1	db	'@/home/anderson/documents/test.odt',0

	node:	istruc	FLFS_Node
;		at	FLFS_Node.data,	db	0
;		at	FLFS_Node.data,	db	0xC3
	iend

	sector	times 8192 db 'X'
	asd	db	0

;	end	db	10,10,10,10,10,10,10,10,10,10

	; Align kernel size to 128k
		times (2016-2-17)*512 - ($ - $$) db 0x66	; reserve 256-2=254 sectors (31744 bytes), 2 sectors for MBR & VBR
		; !!!!!!!!! 17 = IMAGE.HDD SIZE / 512
;		db 13,10,'Hello, world!',13,10,0
;		times 1000 db 0
