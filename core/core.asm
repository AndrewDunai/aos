;db	'0','1','2','3'

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Do not edit these lines!
;jmp	start
;start:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;	int	19h

;	mov	cs,	eax

	push	ebp
	mov	ebp,	eax
	mov	es,	eax


;;	xor	cx,	cx
;;repeat:	mov	esi,	ebp
;;	add	esi,	welcome_to_core
;;	mov	ah,	1h
;;	int	30h

	mov	esi,	welcome_to_core
	add	esi,	ebp
	mov	ah,	1h
	int	30h

;	mov	ebx,	ebp
;	add	ebx,	repeat
;	jmp	ebx

	pop	ebp

	ret

	welcome_to_core	db	"Welcome to core application! Terminating back to kernel...", 13, 10, 0
